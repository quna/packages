# Debian repo
This repo creates a debian repo using gitlab-CI.

## How To
### Add a new `recipe`
- Fork this repo
- Clone the forked repo
- Create a new branch
- move all the recipes to a `temperory subdirectory`
- create a recipe directory 
- inside recipe directory, add ***build*** script with name **`build.sh`**
  > Make sure the deb file gets generated in the same directory
- Pull docker image, if it does not exist
  ```shell
  docker pull bitnami/minideb:bookworm
  ```
- run `docker compose up`
- This should create a new debian package in `target/bookworm`
- move recipes from `temperory subdirectory`, back to recipes directory
- commit, push & send merge request
  > if your copy of the repo is out of sync with upstream, please rebase it with respect to the main branch of upstream repo, before sending merge request

## TODO
- [ ] Support for other(non amd64) architectures
- [ ] Configuration format for environment setup for recipe
- [ ] Prestine environment per build  
  - This should be possible with a combination of bind mount and unionfs
- [ ] Build Periodically
- [ ] Build Only Updated Packages
  - Add `check-update` specification (part of configuration format)
      - standard checks
          - new commits
          - new tags (in particular branch, pattern etc)
      - custom checks (user defined function)
  - In case not updated, download existing deb from repo
- [ ] Add Trigger from source repos
- [ ] In case of repo trigger, build only event source repo
  - avoid chroot in this case
- [ ] Private repo support
- [x] instruction to add new recipe

## References
- [Prebuilt-MPR](https://github.com/makedeb/prebuilt-mpr)
- [vscodium-deb-rpm-repo](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo)
  > gitlab pages based debian repo that downloads latest deb( & rpm) files from github releases & generates deb repo using `reprepro`
- Lorenzo Setale's Gitlab Pages based debian repo
  > - runs on schedule
  > - Makefile based packaging with one makefile per package
  > generates deb repo using `apt-ftparchive`
  - [Blog Post](https://blog.setale.me/2020/08/02/Using-a-GitLab-to-build-a-Debian-Repository/)
  - [Repo](https://gitlab.com/koalalorenzo/apt)
- [Using Docker to Build Debian Packages](https://www.cloudbees.com/blog/using-docker-build-debian-packages) **@CloudBees** *-Mathias Lafeldt*
  > Rakefile based packaging using [`buildtasks`](https://rubygems.org/gems/buildtasks) gem