#!/bin/env bash

set -e

maintainer='Webyfy <info@webyfy.com>'
pkgname=xeventbind
pkgver=0.1.2
pkgrel=1
pkgdesc='A small utillity that runs your script on X11 events'
arch=amd64
section=x11
license=MIT
depends=()
makedepends=(
    'gcc'
    'checkinstall'
    'libx11-dev'
)
url="https://github.com/ritave/xeventbind"
src="https://github.com/ritave/xeventbind"
commit_id=9f66b8d

prepare() {
    :
}

build() {
    cd ${pkgname}/
    make
}

package() {
    cd ${pkgname}/
    echo "${pkgdesc}" > description-pak
    sudo checkinstall -D -y \
    --install=no \
    --fstrans=no \
    --pkgname=${pkgname} \
    --pkgversion=${pkgver} \
    --pkgsource=${src} \
    --pkgarch=${arch} \
    --pakdir="../" \
    --pkgrelease=${pkgrel} \
    --pkggroup=${section} \
    --pkglicense=${license} \
    --maintainer="'${maintainer}'" \
    --requires="'$(IFS=,; echo "${depends[*]}")'" \
    --reset-uids=yes \
    --delspec=yes \
    --deldoc=yes \
    --deldoc=yes
    sudo make uninstall
}

main() {
    sudo apt install -y ${makedepends[*]}
    git clone "${src}"
    src_dir=$(basename ${src} .git)
    pushd ${src_dir}
    git checkout ${commit_id}
    popd

    (prepare)
    (build)
    (package)
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi
