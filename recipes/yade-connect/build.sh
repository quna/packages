#!/bin/env bash
SRC_REPO=https://gitlab.com/webyfy/iot/e-gurukul/yade-connect.git

git clone ${SRC_REPO} src
cd src
latest_tag=$(git describe --tags "$(git rev-list --tags --max-count=1)")
git checkout $latest_tag

bash ./package
